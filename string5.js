function string5(input){ 
    if (input.length === 0){
        return []; 
    }
    
    let result = input.reduce((acc, curr) => {
        acc += curr.trim() + " "; 
        return acc;
    }, ""); 

    return result + '\b.'; 
}

module.exports = string5;