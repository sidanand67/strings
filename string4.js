function string4(dataObj){
    let result = ''; 
    let names = ['first_name', 'middle_name', 'last_name'];

    names.forEach(name => {
        if (dataObj[name]){
            result += (convertStr(dataObj[name]) + ' ')
        }
    }); 
    
    return result; 
}

function convertStr(str){
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase(); 
}

module.exports = string4; 