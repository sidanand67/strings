function string2(ipAddr){
    let splitIP = ipAddr.split('.');

    let isValidIP = splitIP.every(element => { 
        if(Number(element) >= 0 && Number(element) <= 255){
            return true; 
        }
        return false;
     }); 
    
     if (isValidIP){
        return splitIP.map(element => Number(element));  
    }
    return []; 
}

module.exports = string2; 